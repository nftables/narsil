English | [简体中文](README.zh-CN.md)

<p align="center">
    <a href="https://github.com/seatonjiang/narsil#gh-light-mode-only">
        <img src=".github/narsil-light.png#gh-light-mode-only" />
    </a>
    <a href="https://github.com/seatonjiang/narsil#gh-dark-mode-only">
        <img src=".github/narsil-dark.png#gh-dark-mode-only" />
    </a>
</p>

<p align="center">
    <img src="https://img.shields.io/static/v1?style=flat-square&message=Rocky%20Linux&color=15B076&logo=rockylinux&logoColor=FFFFFF&label=">
    <a href="https://github.com/seatonjiang/narsil/issues">
        <img src="https://img.shields.io/github/issues/seatonjiang/narsil?style=flat-square&color=blue">
    </a>
    <a href="https://github.com/seatonjiang/narsil/pulls">
        <img src="https://img.shields.io/github/issues-pr/seatonjiang/narsil?style=flat-square&color=brightgreen">
    </a>
    <a href="https://github.com/seatonjiang/narsil/blob/main/LICENSE">
        <img src="https://img.shields.io/github/license/seatonjiang/narsil?&style=flat-square">
    </a>
</p>

<p align="center">
    <a href="https://github.com/seatonjiang/narsil/issues">Report Bug</a>
    ·
    <a href="https://github.com/seatonjiang/narsil/issues">Request Feature</a>
</p>

<p align="center">System security hardening tool for Rocky Linux</p>

## 💻 Screenshot

### Script Execution

<p align="center">
    <img src=".github/script-execution.png">
</p>

### Login Information

<p align="center">
    <img src=".github/login-information.png">
</p>

### Mount disk

<p align="center">
    <img src=".github/mount-disk.png">
</p>

## ✨ Features

-   Password can be used for a maximum of 30 days.
-   After 30 days of password expiration, the account will be disabled.
-   The interval between two password changes is 1 day.
-   Warning 7 days before password expiration.
-   Set the system default encryption algorithm to SHA512.
-   Set a session timeout policy of 180 seconds.
-   Each created user will be given their own group.
-   The newly created user home directory permissions are changed to 0750.
-   Modify the permissions of the home directory of the stock user to 0750.
-   Hardened OpenSSH config (Some configs need to be done manually).
-   Disable login for users without home directory.
-   Disable login by default for new users.
-   Disable apport and popular-contest statistics for uploading user information.
-   Disable synchronous deletion of user groups when deleting users.

There are many more settings that are not listed, and you can refer to the files in the `scripts` directory for more information.

## 🚀 Quick start

### Step 1: Clone Repo

Make sure the server has git first, otherwise you need to install it using `sudo dnf install -y git`.

```bash
git clone https://github.com/seatonjiang/narsil.git
```

### Step 2: Edit Config File

Go to project directory.

```bash
cd narsil
```

Be sure to authenticate the contents of the config file.

```bash
vi narsil.conf
```

### Step 3: Running Script

If you are root, you can run it directly, if you are a normal user please use `sudo` and you must run the script with `bash`.

```bash
sudo bash narsil.sh
```

## 📝 Config Options

```ini
# Verify Operation
VERIFY='Y'

# Cloud Server Metadata Overlay (DNS Server/NTP Server/Hostname)
METADATA='Y'

# Production Environment Reminder
PROD_TIPS='Y'

# SSH Port Config
SSH_PORT='22'

# Time Zone Config
TIME_ZONE='Asia/Shanghai'

# Hostname Config (not valid if METADATA is Y)
HOSTNAME='rockylinux'

# DNS Server Config (not valid if METADATA is Y)
DNS_SERVER='119.29.29.29 223.5.5.5'

# NTP Server Config (not valid if METADATA is Y)
NTP_SERVER='ntp.tencent.com'

# Docker Config
DOCKER_CE_REPO='http://mirrors.tencent.com/docker-ce/linux/centos/docker-ce.repo'
DOCKER_CE_MIRROR='mirrors.tencent.com'
DOCKER_HUB_MIRRORS='https://hub-mirror.c.163.com'
```

## 🔨 Modular

Narsil contains a number of standalone functions that are not in the auto-executed script and need to be used separately using parameters, which can be viewed using the `bash narsil.sh --help` for all standalone functions.

### Clear Log Files

Clear all syslog files.

```bash
sudo bash narsil.sh -c
```

### Install Docker

Install docker service and set registry mirrors.

```bash
sudo bash narsil.sh -d
```

### Mount Disk

Interactively mount the data disk. Data is priceless, remember to be careful during the operation!

> If the selected hard disk is already mounted, you will be prompted to unmount and format the operation.

```bash
sudo bash narsil.sh -f
```

### Change system hostname

If the parameters of the configuration file have not changed, prioritize getting the metadata.

```bash
sudo bash narsil.sh -h
```

### Change SSH Port

Interactively modify the SSH port.

> The port range is recommended to be between 10000 and 65535.

```bash
sudo bash narsil.sh -p
```

### Uninstall Monitoring Component

Remove the various monitoring components installed into the server by the cloud vendor.

```bash
sudo bash narsil.sh -r
```

### Add swap space

If physical memory is too small, it is recommended to add the swap space.

```bash
sudo bash narsil.sh -s
```

## 📂 Structure

A quick look at the folder structure of this project.

```bash
narsil
├── narsil.sh
├── narsil.conf
├── config
│   └── (some config files)
└── scripts
    └── (some script files)
```

## 🤝 Contributing

We welcome all contributions. You can submit any ideas as Pull Requests or as Issues, have a good time! :)

## 📃 License

The project is released under the GNU General Public License v3.0, see the [LICENCE](https://github.com/seatonjiang/narsil/blob/main/LICENSE) file for details.
