#!/usr/bin/env bash
#
# narsil-clearlogs.sh - Clear all syslog files
# Seaton Jiang <hi@seatonjiang.com>
#
# The latest version of Narsil can be found at:
# https://github.com/seatonjiang/narsil
#
# Copyright (C) 2023 Seaton Jiang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

function narsil_clearlogs()
{
    msg_notic '\n%s\n\n' "Clearing all syslog files, please wait..."

    find /var/log -type f -regex '.*\.[0-9]$' -delete
    find /var/log -type f -regex '.*-[0-9]*$' -delete
    find /var/log -type f -regex '.*\.gz$' -delete

    while IFS= read -r -d '' logfiles
    do
        true > "${logfiles}"
    done < <(find /var/log/ -type f ! -name 'narsil-*' -print0)

    rm -rf /var/log/journal/*
    systemctl restart systemd-journald.service

    dnf clean all >/dev/null 2>&1
    history -c
    history -w

    msg_succ '%s\n\n' "All syslog files have been cleaned up!"
}
