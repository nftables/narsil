#!/usr/bin/env bash
#
# narsil-removeagent.sh - Remove cloud server monitoring component
# Seaton Jiang <hi@seatonjiang.com>
#
# The latest version of Narsil can be found at:
# https://github.com/seatonjiang/narsil
#
# Copyright (C) 2023 Seaton Jiang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

function narsil_removeagent()
{
    msg_notic '\n%s\n\n' "Cloud Server Monitor component is being removed, please wait..."

    if [ -n "$(wget -qO- -t1 -T2 metadata.tencentyun.com)" ]; then
        /usr/local/qcloud/monitor/barad/admin/uninstall.sh >/dev/null 2>&1
        /usr/local/qcloud/stargate/admin/uninstall.sh >/dev/null 2>&1
        /usr/local/qcloud/YunJing/uninst.sh >/dev/null 2>&1
        ./config/tat-agent-uninstall.sh >/dev/null 2>&1
    fi

    msg_succ '%s\n\n' "All monitoring components have been uninstalled!"
}
