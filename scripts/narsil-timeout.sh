#!/usr/bin/env bash
#
# narsil-timeout.sh - Set system timeout to auto logout
# Seaton Jiang <hi@seatonjiang.com>
#
# The latest version of Narsil can be found at:
# https://github.com/seatonjiang/narsil
#
# Copyright (C) 2023 Seaton Jiang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

function narsil_timeout()
{
    msg_info '\n%s\n' "[${STATS}] Set system timeout to auto logout"

    if ! grep -nqri "TMOUT" /etc/profile.d/; then
        echo "export TMOUT=180" > /etc/profile.d/auto-logout.sh
        echo "readonly TMOUT" >> /etc/profile.d/auto-logout.sh
        chmod 0644 /etc/profile.d/auto-logout.sh

        if [[ ${VERIFY} == "Y" ]]; then
            msg_notic '\n%s\n' "• File Content: /etc/profile.d/"
            grep -nri "TMOUT" /etc/profile.d/
        else
            msg_succ '%s\n' "Success, this operation is completed!"
        fi
    else
        msg_succ '%s\n' "Skip, this configuration already exists!"
    fi

    sleep 1

    ((STATS++))
}
