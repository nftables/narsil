#!/usr/bin/env bash
#
# narsil-sshdconfig.sh - Config OpenSSH (Some configurations need to be done manually)
# Seaton Jiang <hi@seatonjiang.com>
#
# The latest version of Narsil can be found at:
# https://github.com/seatonjiang/narsil
#
# Copyright (C) 2023 Seaton Jiang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

function narsil_sshdconfig()
{
    msg_info '\n%s\n' "[${STATS}] Config OpenSSH (Some configurations need to be done manually)"

    cp /etc/ssh/sshd_config /etc/ssh/sshd_config-"$(date +%Y%m%d-%s)".bak
    cp ./config/sshd_config /etc/ssh/sshd_config
    sed -i "s/.*Port.*/Port ${SSH_PORT}/" /etc/ssh/sshd_config
    chown root:root /etc/ssh/sshd_config
    chmod 0600 /etc/ssh/sshd_config
    systemctl restart sshd.service

    if [[ ${VERIFY} == "Y" ]]; then
        msg_notic '\n%s\n' "• File Content: /etc/ssh/sshd_config"
        grep -Ev '^#|^$' /etc/ssh/sshd_config | uniq
    else
        msg_succ '%s\n' "Success, this operation is completed!"
    fi

    sleep 1

    ((STATS++))
}
