#!/usr/bin/env bash
#
# narsil-banner.sh - Add login banner (system info, disk usage and docker status)
# Seaton Jiang <hi@seatonjiang.com>
#
# The latest version of Narsil can be found at:
# https://github.com/seatonjiang/narsil
#
# Copyright (C) 2023 Seaton Jiang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

function narsil_banner()
{
    msg_info '\n%s\n' "[${STATS}] Add login banner (system info, disk usage and docker status)"

    mkdir -p /etc/banner

    cp ./config/banner.sh /etc/profile.d/narsil-banner.sh
    chmod +x /etc/profile.d/narsil-banner.sh

    cp ./config/banner/*-narsil-* /etc/banner/
    chmod +x /etc/banner/*-narsil-*

    if [[ "${PROD_TIPS}" =~ ^['Y','y']$ ]]; then
        echo '/etc/banner/20-narsil-footer' >> /etc/profile.d/narsil-banner.sh
    fi

    msg_succ '%s\n' "Success, this operation is completed!"

    sleep 1

    ((STATS++))
}
