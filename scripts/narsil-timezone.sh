#!/usr/bin/env bash
#
# narsil-timezone.sh - Config the system time zone
# Seaton Jiang <hi@seatonjiang.com>
#
# The latest version of Narsil can be found at:
# https://github.com/seatonjiang/narsil
#
# Copyright (C) 2023 Seaton Jiang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

function narsil_timezone()
{
    msg_info '\n%s\n' "[${STATS}] Config system time zone"

    timedatectl set-timezone "${TIME_ZONE}"
    timedatectl set-local-rtc 0

    if [[ ${VERIFY} == "Y" ]]; then
        msg_notic '\n%s\n' "• Check time zone"
        ls -la /etc/localtime
    else
        msg_succ '%s\n' "Success, this operation is completed!"
    fi

    sleep 1

    ((STATS++))
}
