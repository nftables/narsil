#!/usr/bin/env bash
#
# narsil-limits.sh - Config ulimit for high concurrency situations
# Seaton Jiang <hi@seatonjiang.com>
#
# The latest version of Narsil can be found at:
# https://github.com/seatonjiang/narsil
#
# Copyright (C) 2023 Seaton Jiang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

function narsil_limits()
{
    msg_info '\n%s\n' "[${STATS}] Config ulimit for high concurrency situations"

    if ! grep -qnri "# Narsil Limits Config" /etc/security/limits.conf; then
        sed -i 's/^# End of file*//' /etc/security/limits.conf
        {
            echo '# Narsil Limits Config'
            echo '* soft nofile 655350'
            echo '* hard nofile 655350'
            echo '* soft nproc  unlimited'
            echo '* hard nproc  unlimited'
            echo '* soft core   unlimited'
            echo '* hard core   unlimited'
        } > /etc/security/limits.conf
    fi

    if [[ ${VERIFY} == "Y" ]]; then
        msg_notic '\n%s\n' "• File Content: /etc/security/limits.conf"
        grep -Ev '^#|^$' /etc/security/limits.conf | uniq
    else
        msg_succ '%s\n' "Success, this operation is completed!"
    fi

    sleep 1

    ((STATS++))
}
